<?php

declare(strict_types=1);

namespace Drupal\b24\Service;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Render\Renderer;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The service contains helper methods for working with b24 settings forms.
 */
final class FormHelper {

  use StringTranslationTrait;

  /**
   * The Bitrix24 REST manager.
   *
   * @var \Drupal\b24\Service\RestManager
   */
  protected RestManager $b24RestManager;

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected ConfigManagerInterface $configManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * Constructs a FormHelper object.
   */
  public function __construct($b24_rest_manager, $config_manager, $renderer) {
    $this->b24RestManager = $b24_rest_manager;
    $this->configManager = $config_manager;
    $this->renderer = $renderer;
  }

  /**
   * Adds mapping fields to a given form.
   *
   * @param array $form
   *   The form array.
   * @param string $b24_entity
   *   The bitrix24 entity type id.
   * @param \Drupal\Core\Config\Config $config
   *   The config object.
   *
   * @return array
   *   An array of Bitrix 24 fields.
   */
  public function getMappingFields(array &$form, string $b24_entity, Config $config): array {
    $b24_fields = $this->b24RestManager->getFields($b24_entity);
    foreach ($b24_fields as $b24_field_name => $b24_field_properties) {
      if (in_array($b24_field_properties['type'],
          [
            'string',
            'crm_multifield',
            'double',
          ]
        ) && !$b24_field_properties['isReadOnly']) {
        $form[$b24_entity][$b24_field_name] = [
          '#type' => $b24_field_name == 'COMMENTS' ? 'textarea' : 'textfield',
          '#title' => $b24_field_properties['title'],
          '#default_value' => $config->get("{$b24_entity}.{$b24_field_name}"),
          '#required' => $b24_field_properties['isRequired'],
        ];
      }
    }

    return $b24_fields;
  }

  /**
   * Adds mapping fields (with selects) to a given form.
   *
   * @param array $form
   *   The form array.
   * @param array $config
   *   The settings array.
   * @param array $elements
   *   The mapped Drupal entity fields.
   * @param string $parent_element
   *   The name of parent form element.
   * @param array $extra_tokens
   *   Tokens to use in the fields.
   *
   * @return array
   *   An array of Bitrix 24 fields.
   */
  public function getMappingSelects(array &$form, array $config, array $elements = [], string $parent_element = 'settings', array $extra_tokens = []): array {
    $b24_fields = $this->b24RestManager->getFields('lead');
    $restricted_types = [
      'webform_actions',
      'uuid',
      'language',
      'entity_reference',
    ];
    $options = [];
    foreach ($elements as $element_id => $element) {
      if ($element instanceof BaseFieldDefinition) {
        if (in_array($element->getType(), $restricted_types)) {
          continue;
        }
        $options[$element_id] = $element->getLabel();
      }

      if (is_array($element)) {
        if (!isset($element['#type']) || in_array($element['#type'], $restricted_types) || !isset($element['#title'])) {
          continue;
        }
        $options[$element_id] = $element['#title'];
      }
    }
    $options['custom'] = $this->t('Custom...');

    $tokens = ['site'] + $extra_tokens;

    $token_link = [
      '#theme' => 'token_tree_link',
      '#token_types' => $tokens,
      '#show_restricted' => TRUE,
      '#global_types' => TRUE,
    ];
    $token_link = $this->renderer->render($token_link);
    foreach ($b24_fields as $b24_field_name => $b24_field_properties) {
      if (in_array($b24_field_properties['type'],
          [
            'string',
            'crm_multifield',
            'double',
          ]
        ) && !$b24_field_properties['isReadOnly']) {
        $form[$b24_field_name] = [
          '#type' => 'select',
          '#options' => $options,
          '#title' => $b24_field_properties['title'],
          '#default_value' => $config[$b24_field_name] ?? '',
          '#empty_option' => $this->t('- None -'),
        ];
        if ($b24_field_properties['isRequired']) {
          $form[$b24_field_name]['#label_attributes']['class'][] = 'form-required';
        }
        $form["{$b24_field_name}_custom"] = [
          '#type' => 'textarea',
          '#title' => $this->t('@name custom value', ['@name' => $b24_field_properties['title']]),
          '#description' => $token_link,
          '#states' => [
            'visible' => [
              '[name="' . $parent_element . '[mapping][' . $b24_field_name . ']"]' => [
                'value' => 'custom',
              ],
            ],
          ],
          '#default_value' => $config["{$b24_field_name}_custom"] ?? '',
        ];
      }
    }

    return $b24_fields;
  }

}
