<?php

declare(strict_types=1);

namespace Drupal\Tests\b24\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Test description.
 *
 * @group b24
 */
final class B24UITest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['b24'];

  /**
   * The test user having access to the module configuration.
   *
   * @var \Drupal\user\Entity\User
   */
  private User $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer b24 configuration']);
  }

  /**
   * Tests top level configuration page availability.
   */
  public function testRouteConfigPageAvailability() {
    $this->drupalGet('/admin/config/b24');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/b24');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests credentials form availability.
   */
  public function testCredentialsForm(): void {
    $this->drupalGet('/admin/config/b24/credentials');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/b24/credentials');
    $this->assertSession()->statusCodeEquals(200);

    $this->submitForm([
      'site' => 'tst.bitrix24.ru',
    ], 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->fieldValueEquals(
      'site',
      'tst.bitrix24.ru',
    );
    $config = $this->config('b24.settings');
    $this->assertSession()->fieldValueEquals(
      'site',
      $config->get('site'),
    );
  }

  /**
   * Tests settings form availability.
   */
  public function testSettingsForm(): void {
    $this->drupalGet('/admin/config/b24/settings');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/b24/settings');
    $this->assertSession()->statusCodeEquals(200);

    $this->submitForm([
      'assignee' => 'custom',
      'user_id' => 12,
    ], 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->fieldValueEquals(
      'assignee',
      'custom',
    );
    $this->assertSession()->fieldValueEquals(
      'user_id',
      '12',
    );
    $config = $this->config('b24.default_settings');
    $this->assertSession()->fieldValueEquals(
      'assignee',
      $config->get('assignee'),
    );
    $this->assertSession()->fieldValueEquals(
      'user_id',
      $config->get('user_id'),
    );
  }

}
