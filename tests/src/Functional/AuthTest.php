<?php

declare(strict_types=1);

namespace Drupal\Tests\b24\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group b24
 */
final class AuthTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['b24'];

  /**
   * Test callback.
   */
  public function testAuthPageAvailability(): void {
    $admin_user = $this->drupalCreateUser(['administer b24 configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet(Url::fromRoute('b24.auth'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('No bitrix24 account configured');
  }

}
