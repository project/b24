<?php

namespace Drupal\b24_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Token;
use Drupal\b24\Service\FormHelper;
use Drupal\b24\Service\RestManager;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "b24_webform_handler",
 *   label = @Translation("Bitrix24 webform handler"),
 *   category = @Translation("Bitrix24"),
 *   description = @Translation("Sends submission data to bitrix24."),
 *   cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
final class B24WebformHandler extends WebformHandlerBase {

  /**
   * The bitrix24 rest manager.
   *
   * @var \Drupal\b24\Service\RestManager
   */
  protected RestManager $b24RestManager;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $token;

  /**
   * The Bitrix24 form helper.
   *
   * @var \Drupal\b24\Service\FormHelper
   */
  protected FormHelper $formHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->b24RestManager = $container->get('b24.rest_manager');
    $instance->token = $container->get('token');
    $instance->formHelper = $container->get('b24.form_helper');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $webform = $this->webform;

    $form['mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fields mapping'),
      '#states' => [
        'visible' => [
          '[name="settings[mapping]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $this->getFields($form['mapping'], $webform);

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['mapping'] = $form_state->getValue('mapping');
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $webform = $webform_submission->getWebform();
    $state = $webform->getSetting('results_disabled') ? WebformSubmissionInterface::STATE_COMPLETED : $webform_submission->getState();
    if ($state !== 'completed') {
      return;
    }

    $mapping = $this->configuration['mapping'];
    if (!$mapping) {
      return;
    }
    $values = $webform_submission->getData();
    $fields = [];
    $field_definitions = $this->b24RestManager->getFields('lead');
    foreach ($mapping as $ext_field => $int_field) {
      if (strstr($ext_field, '_custom')) {
        continue;
      }
      if ($int_field && $int_field !== 'custom') {
        $value = $values[$int_field];
      }
      else {
        $override = $mapping["{$ext_field}_custom"] ?? '';
        $value = $this->token
          ->replace($override,
            ['webform' => $webform, 'webform_submission' => $webform_submission]);
      }

      if ($field_definitions[$ext_field]['type'] == 'crm_multifield') {
        $value = [['VALUE' => $value, 'VALUE_TYPE' => 'HOME']];
      }
      $fields[$ext_field] = $value;
    }

    $this->b24RestManager->addLead($fields);
  }

  /**
   * Return configuration fields according to those provided by Bitrix24.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\webform\WebformInterface $webform
   *   The currently processed webform.
   *
   * @return array|mixed
   *   An array of Bitrix 24 fields.
   */
  private function getFields(array &$form, WebformInterface $webform) {
    $config = $this->configuration['mapping'] ?? [];
    $elements = $webform->getElementsDecodedAndFlattened();

    return $this->formHelper->getMappingSelects($form, $config, $elements, 'settings', ['webform', 'webform_submission']);
  }

}
