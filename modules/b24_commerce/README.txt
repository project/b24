Bitrix24 CRM works in two modes: classic and simplified.
Classic is the most understandable for us: a new order on the site creates a lead.
In the simplified mode, a lead is also created, but it is immediately exported
to a deal + contact pair.
