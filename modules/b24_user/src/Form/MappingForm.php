<?php

namespace Drupal\b24_user\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\b24\Service\FormHelper;
use Drupal\b24\Service\RestManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides form for configuring fields mapping for users.
 */
class MappingForm extends ConfigFormBase {

  /**
   * Drupal\b24\Service\RestManager definition.
   *
   * @var \Drupal\b24\Service\RestManager
   */
  protected RestManager $b24RestManager;

  /**
   * The b24 settings form helper.
   *
   * @var \Drupal\b24\Service\FormHelper
   */
  protected FormHelper $formHelper;

  /**
   * Constructs a new MappingForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    RestManager $b24_rest_manager,
    FormHelper $b24_form_helper,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->b24RestManager = $b24_rest_manager;
    $this->formHelper = $b24_form_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('b24.rest_manager'),
      $container->get('b24.form_helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      "b24_user.mapping",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'b24_user_mapping_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $token_link = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['profile', 'user'],
      '#show_restricted' => TRUE,
      '#global_types' => FALSE,
    ];

    $form['token_tree_top'] = $token_link;
    $b24_fields = $this->getFields($form);
    $form['token_tree_bottom'] = $token_link;

    $form_state->addBuildInfo('ext_fields', $b24_fields);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Return fields according to those provided by Bitrix24.
   *
   * @param array $form
   *   The form array.
   *
   * @return array
   *   The fields list.
   */
  private function getFields(array &$form): array {
    $config = $this->config("b24_user.mapping");
    return $this->formHelper->getMappingFields($form, 'contact', $config);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->cleanValues();
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable("b24_user.mapping");
    foreach ($values as $name => $value) {
      $config->set("contact.$name", $value);
    }
    $config->save();

    $this->configFactory->getEditable('b24_user.field_types')->setData($form_state->getBuildInfo()['ext_fields'])->save();
  }

}
